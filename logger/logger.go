package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

var HOST = "172.22.156.172"
// var HOST = "localhost"
var PORT = "1234"
var TYPE = "tcp"
var totalBandwidth = 0

// create map[conn] = name
var clientName = make(map[net.Conn]string)

// adapted from https://www.golinuxcloud.com/golang-tcp-server-client
func main() {
	//./logger 1234
	// parse arguments
	arguments := os.Args
	if len(arguments) != 2 {
		fmt.Println("Please provide 1 argument")
		return
	}

	PORT := arguments[1]

	fmt.Fprintf(os.Stdout, "%.6f - logger started\n", float64(time.Now().UnixMicro())/1e6)

	//1610688413.743385 - node1 connected
	//1610688413.782391 node1 ce783874ba65a148930de32704cd4c809d22a98359f7aed2c2085bc1bd10f096
	listen, err := net.Listen(TYPE, HOST+":"+PORT)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	// close the listener after the server terminates
	defer listen.Close()

	// open log/bandwidth.log
	bandwidthFile, err := os.OpenFile("scenario1/bandwidth.log", os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	// close the file after the server terminates
	defer bandwidthFile.Close()

	// each second record the total bandwidth
	go func() {
		for {
			time.Sleep(time.Second)
			fmt.Fprintf(bandwidthFile, "%d %d\n", time.Now().Unix(), totalBandwidth)
			totalBandwidth = 0
		}
	}()

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		go handleRequest(conn)
	}
}

func handleRequest(conn net.Conn) {
	// close the connection after the server terminates
	defer conn.Close()

	// parse the name of the client
	buffer, err := bufio.NewReader(conn).ReadBytes('\n')
	if err != nil {
		// fail to process client name, print error
		fmt.Println("Failed to process client name" + err.Error())
		return
	}

	// 1610688413.743385 - node1 connected
	// log to stdout seconds since 1970 - name of the client
	fmt.Fprintf(os.Stdout, "%.6f - %s connected\n",
		float64(time.Now().UnixMicro())/1e6, string(buffer[:len(buffer)-1]))
	// get rid of trailing newline and add to map
	clientName[conn] = string(buffer[:])
	if clientName[conn][len(clientName[conn])-1] == '\n' {
		clientName[conn] = clientName[conn][:len(clientName[conn])-1]
	}

	// open log/clientName[conn]
	logfile, err := os.OpenFile("scenario1/"+clientName[conn]+".log", os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	// close the file after the server terminates
	defer logfile.Close()

	// for each second, log the delay to logfile
	// delay = 0.0
	//go func() {
	//	for {
	//		time.Sleep(time.Second)
	//		fmt.Fprintf(logfile, "%.6f %v\n", float64(time.Now().UnixMicro())/1e6, delay)
	//	}
	//}()

	// parse each line of the msg received
	for {
		// read each line sent to server
		buffer, err := bufio.NewReader(conn).ReadBytes('\n')
		if err != nil {
			// client disconnected, print error
			fmt.Println(err.Error())
			return
		}

		// add buffer length to totalBandwidth
		totalBandwidth += len(buffer)

		//1610688413.782391 node1 ce783874ba65a148930de32704cd4c809d22a98359f7aed2c2085bc1bd10f096

		// log to stdout the msg received
		//clientTime, err := strconv.ParseFloat(string(buffer[:18]), 64)

		// print nodeName time.Now().Second(), delay
		fmt.Fprintf(logfile, "%d %v %s\n", time.Now().Unix(), time.Now().UnixNano(), string(buffer[:18]))

		fmt.Fprintf(os.Stdout, "%s", string(buffer[:]))
	}

	//responseStr := fmt.Sprintf("Your message is: %v. Received curTime: %v", string(buffer[:]), curTime)
	//conn.Write([]byte(responseStr))
}
