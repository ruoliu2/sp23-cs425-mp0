# sp23-cs425-mp0

## MP0 Report



1. **Basic info**

Members: Ruo Liu (ruoliu2), Chang Tessa (chingc4) 

git commit : 1dca0cf89978f555b3af1cbf95c3861e90a35a30

URL: https://gitlab.engr.illinois.edu/ruoliu2/sp23-cs425-mp0.git

group number: 52

 

2. **Instruction to running code**

Server/Logger Node

1. Get your ip address and replace the HOST value in the logger/logger.go file. 

2. Create a scenario1 dir in your working directory as the default dir for the dump files, or change the dir name in logger/logger.go to your desired dir.

3. Type **make** in terminal and Makefile will compile the logger and node executable in the bin dir. Also, this will start the server on the default port 1234, or you could use 

   ```bash
   $ bin/logger [your desired port]
   ```



Client Node

1. Git push and pull on your other client node to get the executable and run the following command

```bash
$ python3 -u generator.py [rate] | ./bin/node [name] [host ip] [port]
```

replace the rate with your desired rate, replace name with your node name, replace host ip with the server ip, and the port you just launched your server node on, Use Ctrl+C to terminate



Graph

1. change the NUM_OF_NODE and TIME in the ipynb file and rerun every section
   1. NUM_OF_NODE number of nodes connected
   2. TIME time period you with to run your logger



3. **Delay and Bandwidth Measurement**

   1. Delay

      logger will generate N files in the scenario1 directory labeled node{N}.log each contains logs for each node. The data format is as follows.

      ```
      [serverTime(in Sec)] [serverTime(in Nanosec)] [clientTime]
      ```

      First two are serverTime that received the msg in different units, the last is the clientTime parsed from the msg send to server

      we get the delay by **serverTime(Nano) - clientTime**

       

   2. Bandwidth

      During logging, logger will output the totalBandwidth every second and set it to zero. And output to the bandwidth.log file with the format

      ```
      [serverTime(in Sec)] [totalBandwidth(Bytes)]
      ```

      the total bandwidth each second is what we want



4. **Graphs of the evaluation**

![scenario1](scenario1.png)

Figure 1: 3 nodes, 2.0 Hz each, running for 100 seconds



![scenario2](scenario2.png)

Figure 2: 8 nodes, 5.0 Hz each, running for 100 seconds