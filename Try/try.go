package main

import (
	"fmt"
	"os"
)

func main() {
	clientTime := 1675793846224837
	serverTime := 1675793846.224836
	delay := float64(clientTime)/1e6 - serverTime
	fmt.Fprintf(os.Stdout, "%v\n", delay)
	fmt.Println(delay)
	// open file ../log/bandwidth.log
	// if directory ../log does not exist, create it
	// if file bandwidth.log does not exist, create it
	//if _, err := os.Stat("log"); os.IsNotExist(err) {
	//	fmt.Println("Directory does not exist")
	//	err := os.Mkdir("log", 0755)
	//	if err != nil {
	//		fmt.Println(err.Error())
	//		os.Exit(1)
	//	}
	//}
	//if _, err := os.Stat("log/bandwidth.log"); os.IsNotExist(err) {
	//	fmt.Println("File does not exist")
	//	f, err := os.Create("log/bandwidth.log")
	//	if err != nil {
	//		fmt.Println(err.Error())
	//		os.Exit(1)
	//	}
	//	f.Close()
	//}
	//bandwidthFile, err := os.OpenFile("../log/bandwidth.log", os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	//if err != nil {
	//	fmt.Println(err.Error())
	//	os.Exit(1)
	//
	//}
	//// close the file after the server terminates
	//defer bandwidthFile.Close()
	//
	//bandwith := 0

	//go func() {
	//	for {
	//		time.Sleep(time.Second * 2)
	//		bandwith += 1
	//		fmt.Fprintf(bandwidthFile, "%.6f %d\n", float64(time.Now().UnixMicro())/1e6, bandwith)
	//	}
	//}()
	//
	//for {
	//	time.Sleep(time.Second)
	//	fmt.Println("Main loop")
	//}
}
